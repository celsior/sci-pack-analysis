#!/bin/bash

cd $1

asy -fpdf -o "gr" -c 'string fname="gr.dat"; int xcol=0; int ycol=1; pair lx=(0,300); pair ly=(0,3.5); string xlabel="r, мкм"; string ylabel="g(r)"; include "../plot_curve.asy"; '
asy -fpdf -o "Al_gr" -c 'string fname="Al_gr.dat"; int xcol=0; int ycol=1; pair lx=(0,300); pair ly=(0,2.5); string xlabel="r, мкм"; string ylabel="g(r)"; include "../plot_curve.asy"; '

asy -fpdf -o "Lr" -c 'string fname="gr.dat"; int xcol=0; int ycol=3; pair lx=(0,300); pair ly=(-5,10); string xlabel="r, мкм"; string ylabel="L(r)"; include "../plot_curve.asy"; '
asy -fpdf -o "Al_Lr" -c 'string fname="Al_gr.dat"; int xcol=0; int ycol=3; pair lx=(0,300); pair ly=(-5,12); string xlabel="r, мкм"; string ylabel="L(r)"; include "../plot_curve.asy"; '


asy -fpdf -o "cc_dist" -c 'string fname="cc_dist.dat"; pair lx=(0,30); pair ly=(0,0.5); include "../plot_distr.asy"; '
asy -fpdf -o "pp_dist" -c 'string fname="pp_dist.dat"; pair lx=(0,5); pair ly=(0,1.2); include "../plot_distr.asy"; '

asy -fpdf -o "Al_cc_dist" -c 'string fname="Al_cc_dist.dat"; pair lx=(0,60); pair ly=(0,0.1); include "../plot_distr.asy"; '
asy -fpdf -o "Al_pp_dist" -c 'string fname="Al_pp_dist.dat"; pair lx=(0,40); pair ly=(0,0.4); include "../plot_distr.asy"; '

asy -fpdf -o "cls_dist" -c 'string fname="cls_dist.dat"; pair lx=(0,40); pair ly=(0,0.1); include "../plot_distr.asy"; '

asy -fpdf -o "pockets_dist" -c 'string fname="pockets_dist.dat"; pair lx=(0,10); pair ly=(0,2); include "../plot_distr.asy"; '
