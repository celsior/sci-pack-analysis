#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <tuple>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>

#include <CGAL/IO/Geomview_stream.h>
#include <CGAL/IO/Triangulation_geomview_ostream_3.h>
#include <CGAL/IO/Triangulation_geomview_ostream_2.h>

#include <CGAL/Point_3.h>
#include <CGAL/Vector_3.h>
#include <CGAL/Bbox_3.h>
#include <CGAL/squared_distance_3.h>

#include <armadillo>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_3<double, K> Vb;
typedef CGAL::Triangulation_data_structure_3<Vb> Tds;
typedef CGAL::Delaunay_triangulation_3<K, Tds> Delaunay;
typedef Delaunay::Point Point;
typedef CGAL::Bbox_3 Box;
typedef CGAL::Vector_3<CGAL::Epick> Vector;

typedef std::vector< std::pair<Point, double> > ParticlesVector;

double sqr(double v)
{
  return v*v;
}

double sign(double v)
{
  if (v == 0)
    return 0;
  else if (v < 0)
    return -1;
  return 1;
}

bool flip(double* v, double r, double D, double lo, double hi)
{
  if (*v - r < lo + D)
    {
      *v = hi + (*v - lo);
      return true;
    }
  else if (*v + r > hi - D)
    {
      *v = lo - (hi - *v);
      return true;
    }
  else
    return false;
}


void read_particles(const char* fname, double side, double min_radius, ParticlesVector& points)
{
  std::ifstream str(fname);
  if (! str.is_open())
    throw std::runtime_error("Cannot open file");

  std::string tmp;
  char rectype = 0;
  str >> rectype;
  while (!str.eof() && (rectype != 'E') )
    {
      if (rectype == 'E')
        break;
      else if (rectype != 'P')
        throw std::runtime_error("Malformed file");

      std::string tag, distr;
      double radius;
      bool fixed;
      double x, y, z, nx, ny, nz;

      str >> tag >> distr >> x >> y >> z >> radius >> fixed;
      if (radius < min_radius)
        {
          str >> rectype;
          continue;
        }

      points.push_back(std::make_pair(Point(x,y,z), radius));

      nx = x;
      if (flip(&nx, radius, side, 0, side))
        points.push_back(std::make_pair(Point(nx,y,z), radius));
      ny = y;
      if (flip(&ny, radius, side, 0, side))
        points.push_back(std::make_pair(Point(x,ny,z), radius));
      nz = z;
      if (flip(&nz, radius, side, 0, side))
        points.push_back(std::make_pair(Point(x,y,nz), radius));
      nx = x; ny = y;
      if (flip(&nx, radius, side, 0, side) && flip(&ny, radius, side, 0, side))
        points.push_back(std::make_pair(Point(nx,ny,z), radius));
      nx = x; nz = z;
      if (flip(&nx, radius, side, 0, side) && flip(&nz, radius, side, 0, side))
        points.push_back(std::make_pair(Point(nx,y,nz), radius));
      ny = y; nz = z;
      if (flip(&ny, radius, side, 0, side) && flip(&nz, radius, side, 0, side))
        points.push_back(std::make_pair(Point(x,ny,nz), radius));
      nx = x; ny = y; nz = z;
      if (flip(&nx, radius, side, 0, side) && flip(&ny, radius, side, 0, side) && flip(&nz, radius, side, 0, side))
        points.push_back(std::make_pair(Point(nx,ny,nz), radius));

      str >> rectype;
    }
}

double field_value(const ParticlesVector& particles, const Point& p)
{
  double ret = 0;
  for (auto& c : particles)
    {
      double d = sqrt( sqr(c.first.x() - p.x()) +  sqr(c.first.y() - p.y()) +  sqr(c.first.z() - p.z()) ) - c.second;
      if (d < 0)
        return -1;
      ret += 1.0/d;
    }
  return ret;
}


bool in_triangle(const Point& p, const Point& v1, const Point& v2, const Point& v3)
{
  double alpha = ((v2.y() - v3.y())*(p.x() - v3.x()) + (v3.x() - v2.x())*(p.y() - v3.y())) /
    ((v2.y() - v3.y())*(v1.x() - v3.x()) + (v3.x() - v2.x())*(v1.y() - v3.y()));
  double beta = ((v3.y() - v1.y())*(p.x() - v3.x()) + (v1.x() - v3.x())*(p.y() - v3.y())) /
    ((v2.y() - v3.y())*(v1.x() - v3.x()) + (v3.x() - v2.x())*(v1.y() - v3.y()));
  double gamma = 1.0 - alpha - beta;
  return (alpha > 0 && beta > 0 && gamma > 0);
}

bool in_tetrahedron(const Point& p, const Point& v1, const Point& v2, const Point& v3, const Point& v4)
{
  arma::mat::fixed<4,4> m0({
      v1.x(), v2.x(), v3.x(), v4.x(),
      v1.y(), v2.y(), v3.y(), v4.y(),
      v1.z(), v2.z(), v3.z(), v4.z(),
        1, 1, 1, 1
  } );

  arma::mat::fixed<4,4> m1({
      p.x(), v2.x(), v3.x(), v4.x(),
      p.y(), v2.y(), v3.y(), v4.y(),
      p.z(), v2.z(), v3.z(), v4.z(),
      1, 1, 1, 1
  } );

  arma::mat::fixed<4,4> m2({
      v1.x(), p.x(), v3.x(), v4.x(),
      v1.y(), p.y(), v3.y(), v4.y(),
      v1.z(), p.z(), v3.z(), v4.z(),
        1, 1, 1, 1
  } );

  arma::mat::fixed<4,4> m3({
      v1.x(), v2.x(), p.x(), v4.x(),
      v1.y(), v2.y(), p.y(), v4.y(),
      v1.z(), v2.z(), p.z(), v4.z(),
        1, 1, 1, 1
  } );

  arma::mat::fixed<4,4> m4({
      v1.x(), v2.x(), v3.x(), p.x(),
      v1.y(), v2.y(), v3.y(), p.y(),
      v1.z(), v2.z(), v3.z(), p.z(),
        1, 1, 1, 1
  } );

  double d0 = arma::det(m0);
  if (d0 == 0)
    return in_triangle(p, v1, v2, v3);

  double d1 = arma::det(m1);
  if (sign(d1) != sign(d0))
    return false;

  double d2 = arma::det(m2);
  if (sign(d2) != sign(d0))
    return false;

  double d3 = arma::det(m3);
  if (sign(d3) != sign(d0))
    return false;

  double d4 = arma::det(m4);
  if (sign(d4) != sign(d0))
    return false;

  return true;
}

void write_particles(const char* fname, const ParticlesVector& particles)
{
  std::ofstream out(fname);
  for (auto& c : particles)
    out << "c " << c.first.x() << "," << c.first.y() << " " << c.second << "\n";
}



void fill_map(const Delaunay& T, double side, arma::cube& f)
{
  int Nx = f.n_rows;
  int Ny = f.n_cols;
  int Nz = f.n_slices;
  double dx = side / Nx;
  double dy = side / Ny;
  double dz = side / Nz;

  for (auto fit = T.finite_cells_begin(); fit !=  T.finite_cells_end(); ++fit)
    {
      auto bb = T.tetrahedron(fit).bbox();

      for (int i = floor(bb.xmin()/dx); i < ceil(bb.xmax()/dx);  ++i)
        for (int j = floor(bb.ymin()/dy); j < ceil(bb.ymax()/dy); ++j)
          for (int k = floor(bb.zmin()/dz); k < ceil(bb.zmax()/dz); ++k)
            {
              Point p(side/Nx*i, side/Ny*j, side/Nz*k);
              if (p.x() < 0 || p.y() < 0 || p.z() < 0 || p.x() > side || p.y() > side || p.z() > side || i >= Nx || j >= Ny || k >= Nz)
                continue;

              if (! in_tetrahedron(p,  T.tetrahedron(fit)[3],  T.tetrahedron(fit)[2],  T.tetrahedron(fit)[1],  T.tetrahedron(fit)[0]))
                {
                  Point p2(side/Nx*i + dx/4, side/Ny*j + dy/4, side/Nz*k + dz/4);
                  if (! in_tetrahedron(p2,  T.tetrahedron(fit)[3],  T.tetrahedron(fit)[2],  T.tetrahedron(fit)[1],  T.tetrahedron(fit)[0]))
                    continue;
                }

              double ret = 1e25;
              for (int n = 0; n < 4; ++n)
                {
                  Point cr = fit->vertex(n)->point();
                  double r = fit->vertex(n)->info();

                  double d = sqrt( sqr(cr.x() - side/Nx*i) +  sqr(cr.y() - side/Ny*j) +  sqr(cr.z() - side/Nz*k) ) - r;
                  if (d < 0)
                    {
                      ret = -1e25;
                      break;
                    }
                  else
                    ret = std::min(ret, d);
                }
              if (ret > 0)
                for (int n = 0; n < 4; ++n)
                  {
                    Point cr = T.mirror_vertex(fit, n)->point();
                    double r = T.mirror_vertex(fit, n)->info();

                    double d = sqrt( sqr(cr.x() - side/Nx*i) +  sqr(cr.y() - side/Ny*j)  +  sqr(cr.z() - side/Nz*k) ) - r;
                    if (d < 0)
                      {
                        ret = -1e25;
                        break;
                      }
                    else
                      ret = std::min(ret, d);
                  }
              if (f(i,j,k) != 0)
                std::cerr << "overwrite " << i << " " << j << " " << k << " " << f(i,j,k) << " " << ret << "\n";
              f(i,j,k) = (ret > 0 ? ret : -1);
            }
    }
}

double sector_area(const Point& c, const Point& a, const Point& b, double r)
{
  Vector v1 (a,c);
  Vector v2 (b,c);

  double angle = acos(v1*v2 / sqrt(v1.squared_length()*v2.squared_length()));
  angle = std::min(angle, 2*M_PI - angle);
  return sqr(r) * angle / 2;
}

double segment_area(double r, double d)
{
  double angle = 2*acos(d/r);
  return sqr(r)/2 * (angle - sin(angle));
}


double point_line_dist(const Point& p, const Point& a, const Point& b)
{
  Vector ln (a,b);
  Vector ap (a,p);

  ln = ln / sqrt(ln.squared_length());

  return sqrt( (ap - ap*ln*ln).squared_length() );
}

void mark_borders(const Delaunay& T, double side, double min_area, arma::cube& f)
{
  int Nx = f.n_rows;
  int Ny = f.n_cols;
  int Nz = f.n_slices;
  double dx = side / Nx;
  double dy = side / Ny;
  double dz = side / Nz;


  for (auto fit = T.finite_facets_begin(); fit !=  T.finite_facets_end(); ++fit)
    {
      auto tr = T.triangle(*fit);
      decltype(fit->first->vertex(0)) v[] = {fit->first->vertex( (fit->second+1)%4 ),
                                             fit->first->vertex( (fit->second+2)%4 ),
                                             fit->first->vertex( (fit->second+3)%4 )};

      double area = sqrt(tr.squared_area());
      for (int i = 0; i < 3; ++i)
        {
          double r = v[i]->info();
          area -= sector_area(v[i]->point(), v[(i+1)%3]->point(), v[(i+2)%3]->point(), r);
          double d = point_line_dist(v[i]->point(), v[(i+1)%3]->point(), v[(i+2)%3]->point());
          if (d < r)
            area += segment_area(r, d);
        }
      if (area > min_area)
        continue;

      auto bb = tr.bbox();
      for (int i = floor(bb.xmin()/dx); i < ceil(bb.xmax()/dx);  ++i)
        for (int j = floor(bb.ymin()/dy); j < ceil(bb.ymax()/dy); ++j)
          for (int k = floor(bb.zmin()/dz); k < ceil(bb.zmax()/dz); ++k)
            {
              Point p(side/Nx*i, side/Ny*j, side/Nz*k);
              if (p.x() < 0 || p.y() < 0 || p.z() < 0 || p.x() > side || p.y() > side || p.z() > side || i >= Nx || j >= Ny || k >= Nz)
                continue;

              Box b(side/Nx*i, side/Ny*j, side/Nz*k, side/Nx*(i+1), side/Ny*(j+1), side/Nz*(k+1));
              if (CGAL::do_intersect(tr, b))
                f(i,j,k) = -2;
            }
    }
}

void mark_particles(const ParticlesVector& particles, double side, arma::cube& f)
{
  int Nx = f.n_rows;
  int Ny = f.n_cols;
  int Nz = f.n_slices;
  double dx = side / Nx;
  double dy = side / Ny;
  double dz = side / Nz;

  for (auto& p : particles)
    {
      for (int i = floor((p.first.x()-p.second)/dx); i < ceil((p.first.x()+p.second)/dx);  ++i)
        for (int j = floor((p.first.y()-p.second)/dy); j < ceil((p.first.y()+p.second)/dy);  ++j)
          for (int k = floor((p.first.z()-p.second)/dz); k < ceil((p.first.z()+p.second)/dz);  ++k)
            {
              Point pt(side/Nx*i, side/Ny*j, side/Nz*k);
              if (pt.x() < 0 || pt.y() < 0 || pt.z() < 0 || pt.x() > side || pt.y() > side || pt.z() > side || i >= Nx || j >= Ny || k >= Nz)
                continue;

              if (CGAL::squared_distance(p.first, pt) < p.second)
                f(i,j,k) = -1;
            }
    }
}

int main(int argc, const char** argv)
{
  const double side = atof(argv[1]);
  const int N = atoi(argv[2]);
  const double min_area = atof(argv[3]);
  const double min_radius = atof(argv[4])/2;
  const double dx = side/N;

  ParticlesVector particles;
  read_particles(argv[5], side, min_radius, particles);

  Delaunay T;
  T.insert( particles.begin(), particles.end() );

  arma::cube f(N, N, N);
  f.fill(0);
  //fill_map(T, side, f);
  mark_borders(T, side, min_area, f);
  mark_particles(particles, side, f);

  std::ofstream out(argv[6]);

  for (int i = 0; i < f.n_cols; ++i)
    for (int j = 0; j < f.n_rows; ++j)
      for (int k = 0; k < f.n_slices; ++k)
        out << i << " " << j << " " << k << " " << f(i,j,k) << "\n";

  return 0;
}
