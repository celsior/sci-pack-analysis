#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <math.h>
#include <stdexcept>

double sqr(double v)
{
  return v*v;
}

double cbk(double v)
{
  return v*v*v;
}


struct PointData
{
  int N;
  int nn;
  double nn_dist;
  double nn_dist_cc;
  int c;

  PointData(int N)
    : N(N), nn(-1), nn_dist(1e15), nn_dist_cc(1e15), c(0) {}
};

struct Point
{
  double x;
  double y;
  double z;
  double r;
  PointData* dt;

  Point(double x, double y, double z, double r, PointData* dt)
    : x(x), y(y), z(z), r(r), dt(dt) {}

  double dist(const Point& p)
  {
    return sqrt(sqr(x-p.x) + sqr(y-p.y) + sqr(z-p.z)) - r - p.r;
  }

  double dist_cc(const Point& p)
  {
    return sqrt(sqr(x-p.x) + sqr(y-p.y) + sqr(z-p.z));
  }
};

std::ostream& operator<<(std::ostream& str, const Point& p)
{
  str << p.dt->N << " " << p.x << " " << p.y << " " << p.z;
  return str;
}

typedef std::vector<Point> ParticlesVector;



bool flip(double* v, double r, double D, double lo, double hi)
{
  if (*v - r < lo + D)
    {
      *v = hi + (*v - lo);
      return true;
    }
  else if (*v + r > hi - D)
    {
      *v = lo - (hi - *v);
      return true;
    }
  else
    return false;
}


void read_particles(const char* fname, double side, ParticlesVector& points)
{
  std::ifstream str(fname);
  if (! str.is_open())
    throw std::runtime_error("Cannot open file");

  std::string tmp;
  char rectype = 0;
  str >> rectype;
  int N = 0;
  while (!str.eof() && (rectype != 'E') )
    {
      if (rectype == 'E')
        break;
      else if (rectype != 'P')
        throw std::runtime_error("Malformed file");

      std::string tag, distr;
      double radius;
      bool fixed;
      double x, y, z, nx, ny, nz;

      str >> tag >> distr >> x >> y >> z >> radius >> fixed;
      PointData* dt = new PointData(++N);
      points.push_back(Point(x,y,z,radius,dt));

      nx = x;
      if (flip(&nx, radius, 0, 0, side))
        points.push_back(Point(nx,y,z,radius,dt));
      ny = y;
      if (flip(&ny, radius, 0, 0, side))
        points.push_back(Point(x,ny,z,radius,dt));
      nz = z;
      if (flip(&nz, radius, 0, 0, side))
        points.push_back(Point(x,y,nz,radius,dt));
      nx = x; ny = y;
      if (flip(&nx, radius, 0, 0, side) && flip(&ny, radius, 0, 0, side))
        points.push_back(Point(nx,ny,z,radius,dt));
      nx = x; nz = z;
      if (flip(&nx, radius, 0, 0, side) && flip(&nz, radius, 0, 0, side))
        points.push_back(Point(nx,y,nz,radius,dt));
      ny = y; nz = z;
      if (flip(&ny, radius, 0, 0, side) && flip(&nz, radius, 0, 0, side))
        points.push_back(Point(x,ny,nz,radius,dt));
      nx = x; ny = y; nz = z;
      if (flip(&nx, radius, 0, 0, side) && flip(&ny, radius, 0, 0, side) && flip(&nz, radius, 0, 0, side))
        points.push_back(Point(nx,ny,nz,radius,dt));

      str >> rectype;
    }
}

void eq_unify(int e1, int e2, std::vector<int>& eq)
{
  if (e1 != 0 && e2 != 0 && (e1 != e2))
    for (int n = 0; n < eq.size(); ++n)
      if (eq[n] == e1)
        eq[n] = e2;
}


int main(int argc, const char** argv)
{
  const double side = atof(argv[1]);
  const double sep_dist = atof(argv[2]);

  ParticlesVector particles;
  read_particles(argv[3], side, particles);

  std::vector<int> eq = {0};
  int max_label = 0;

  for (auto& p : particles)
    {
      if (p.x < 0 || p.x > side || p.y < 0 || p.y > side || p.z < 0 || p.z > side)
        continue;

      for (auto& p1 : particles)
        {
          if (p.dt->N == p1.dt->N)
            continue;

          double d = p.dist(p1);
          if (d < p.dt->nn_dist)
            {
              p.dt->nn_dist = d;
              p.dt->nn = p1.dt->N;
            }

          double d_cc = p.dist_cc(p1);
          if (d_cc < p.dt->nn_dist_cc)
            p.dt->nn_dist_cc = d_cc;

          if (d < sep_dist)
            {
              if (p.dt->c == 0 && p1.dt->c == 0)
                {
                  ++max_label;
                  eq.push_back(max_label);
                  p.dt->c = max_label;
                  p1.dt->c = max_label;
                }
              else if (p.dt->c == 0 && p1.dt->c != 0)
                p.dt->c = p1.dt->c;
              else if (p.dt->c != 0 && p1.dt->c == 0)
                p1.dt->c = p.dt->c;
              else
                eq_unify(p.dt->c, p1.dt->c, eq);
            }
        }
    }


  std::ofstream out_points(argv[4]);
  out_points.precision(16);
  for (auto& p : particles)
    {
      if (p.x < 0 || p.x > side || p.y < 0 || p.y > side || p.z < 0 || p.z > side)
        continue;

      out_points << p.dt->N << " " << p.r << " " << p.dt->nn << " " << p.dt->nn_dist << " " << p.dt->nn_dist_cc << " " << p.dt->c << "\n";
    }

  std::map<int,std::pair<double,int>> cls;
  for (auto& p : particles)
    {
      cls[p.dt->c].first += 4.0/3*M_PI*cbk(p.r);
      cls[p.dt->c].second += 1;
    }

  std::ofstream out_clusters(argv[5]);
  out_clusters.precision(16);
  for (auto& c : cls)
    out_clusters << c.first << " " << c.second.first << " " << c.second.second  << "\n";

  return 0;
}
