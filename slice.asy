include "/home/celsior/study/styles/preamble.asy";

import markers;
import graph;
import palette;
import contour;

size(15cm,15cm,IgnoreAspect);

file in=input("slice_3d").line();
real[][] a=in.dimension(0,0);
real side = 500;
int N = 200;

real dx = side/N;
real dy = side/N;

int xidx(real x)
{
  return round(x/dx);
}

int yidx(real y)
{
  return round(y/dy);
}

real[][] v=new real[N][N];

int maxy = 0;
real maxv = 0;
real minv = 1e25;

for (int i = 0; i < a.length; ++i)
  {
    v[xidx(a[i][0])][yidx(a[i][1])] = a[i][2];

    if (a[i][2] != -1)
      {
        minv = min(minv, a[i][2]);
        maxv = max(maxv, a[i][2]);
      }

    if (yidx(a[i][1]) > maxy)
      maxy = xidx(a[i][0]);
  }

pen[] palette = new pen[32000];
real step = 1.0/32000;
for (int i = 0; i < 32000; ++i)
  {
    real v = i*step;
    palette[i] = rgb(abs(2*v - 0.5), sin(pi*v), cos(pi/2*v));
    //palette[i] = gray(1 - sqrt(v));
  }


//bounds range=image(v, (0,0),(side,side), palette, range=Range(minv, min(maxv, 1)));

bounds range=image(v, (0,0),(side,side), palette);

//draw(contour(v, (0,0), (side,side), new real[]{0.12}));
limits( (0,0), (side,side), Crop );

xaxis(BottomTop,LeftTicks(),above=true);
label(shift(0mm,-6mm)*"$x$, мкм", point(SE));
yaxis(LeftRight,RightTicks(),above=true);
label(shift(0,3mm)*"$y$, мкм", point(NW));


// picture bar;
// palette(bar,"",range,(0,0),(0.5cm, 9cm),Right, palette, PaletteTicks(defaultformat, Step=10));
// attach(bar.fit(),point(E),10E);
// label(shift(6mm,-0mm)*"$T$, K", point(NE));
