#!/usr/bin/python

import sys
import math
import argparse

import numpy as np

parser = argparse.ArgumentParser("")
parser.add_argument("--type", default="dist", choices=["dist", "vol"])
parser.add_argument("--col", default=1, type=int)
parser.add_argument("--bins", default=100, type=int)
parser.add_argument("--show", action='store_true')
parser.add_argument("--normed", action='store_true')
parser.add_argument("--cluster", action='store_true')
parser.add_argument("--mass", action='store_true')
parser.add_argument("--cellvolume", type=float)
parser.add_argument("file")

args = parser.parse_args(sys.argv[1:])

N = args.bins

if not args.cluster:
    d = np.loadtxt(args.file, dtype=float, usecols=[args.col])
    if args.cellvolume:
        d *= args.cellvolume
else:
    dt = np.loadtxt(args.file, dtype=float, usecols=[0, args.col])
    if args.cellvolume:
        dt[1] *= cellvolume
    d = []
    s = 0.0
    f = 0.0
    for k,v in dt:
        s += v
        if k != 0:
            d.append(v)
        else:
            f = v
    print >>sys.stderr, "free ", f, f/s

w = np.ones(len(d))

if args.type == "vol":
    for i in range(len(d)):
        d[i] = (3.0/4.0/math.pi * d[i]) ** (1.0/3)

if args.mass:
    for i in range(len(d)):
        w[i] = d[i]**3


h,edges = np.histogram(d, N, density=args.normed, weights=w)

if args.type == "vol":
    d0 = 0.0
    d1 = 0.0
    d3 = 0.0
    d4 = 0.0
    for i in range(N-1):
        d0 += h[i]*(edges[i+1]-edges[i])
        d1 += h[i]*(edges[i+1]-edges[i])*(edges[i]+edges[i+1])/2
        d3 += h[i]*(edges[i+1]-edges[i])*((edges[i]+edges[i+1])/2)**3
        d4 += h[i]*(edges[i+1]-edges[i])*((edges[i]+edges[i+1])/2)**4
    print >>sys.stderr, d4/d3, d1/d0


for i in range(N-1):
    print edges[i], edges[i+1], h[i]

if args.show:
    import matplotlib.pyplot as plt

    plt.bar(edges[:N], h, edges[1:] - edges[:N])
    plt.show()
