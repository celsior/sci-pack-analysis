#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <armadillo>


void eq_unify(int e1, int e2, std::vector<int>& eq)
{
  if (e1 != 0 && e2 != 0 && (e1 != e2))
    for (int n = 0; n <= eq.size()-1; ++n)
      if (eq[n] == e1)
        eq[n] = e2;
}


void mark_cc(arma::cube& f, arma::Cube<int>& cc_f, std::map<int, int>& w)
{
  int Nx = f.n_rows;
  int Ny = f.n_cols;
  int Nz = f.n_slices;

  std::vector<int> eq = {0};
  int max_label = 0;

  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Ny; ++j)
      for (int k = 0; k < Nz; ++k)
        {
          if (f(i,j,k) < 0)
            continue;

          int li = i == 0 ? 0 : cc_f(i-1, j, k);
          int lj = j == 0 ? 0 : cc_f(i, j-1, k);
          int lk = k == 0 ? 0 : cc_f(i, j, k-1);
          int lx = 0;
          if (li == 0 && lj == 0 && lk == 0)
            {
              ++max_label;
              eq.push_back(max_label);
              lx = max_label;
            }
          else if (li == 0 && lj == 0 && lk != 0)
            {
              lx = lk;
            }
          else if (li == 0 && lj != 0 && lk == 0)
            {
              lx = lj;
            }
          else if (li == 0 && lj != 0 && lk != 0)
            {
              eq_unify(eq[lj], eq[lk], eq);
              lx = lk;
            }
          else if (li != 0 && lj == 0 && lk == 0)
            {
              lx = li;
            }
          else if (li != 0 && lj == 0 && lk != 0)
            {
              eq_unify(eq[li], eq[lk], eq);
              lx = lk;
            }
          else if (li != 0 && lj != 0 && lk == 0)
            {
              eq_unify(eq[li], eq[lj], eq);
              lx = lj;
            }
          else if (li != 0 && lj != 0 && lk != 0)
            {
              eq_unify(eq[li], eq[lj], eq);
              eq_unify(eq[lj], eq[lk], eq);
              lx = lk;
            }

          cc_f(i,j,k) = lx;
        }

  for (int i = 0; i < Ny; ++i)
    for (int j = 0; j < Nz; ++j)
      {
        int li = cc_f(0,i,j);
        int lj = cc_f(Nx-1,i,j);
        if (li != 0 && lj != 0)
          eq_unify(eq[li], eq[lj], eq);
      }

  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Nz; ++j)
      {
        int li = cc_f(i,0,j);
        int lj = cc_f(i,Ny-1,j);
        if (li != 0 && lj != 0)
          eq_unify(eq[li], eq[lj], eq);
      }

  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Ny; ++j)
      {
        int li = cc_f(i,j,0);
        int lj = cc_f(i,j,Nz-1);
        if (li != 0 && lj != 0)
          eq_unify(eq[li], eq[lj], eq);
      }

  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Ny; ++j)
      for (int k = 0; k < Nz; ++k)
      {
        cc_f(i,j,k) = eq[cc_f(i,j,k)];
        w[cc_f(i,j,k)] += 1;
      }
}


int main(int argc, const char** argv)
{
  const int N = atoi(argv[1]);
  arma::cube f(N, N, N);

  std::ifstream in(argv[2]);
  if (! in.is_open())
    throw std::runtime_error("Cannot open file");

  while (! in.eof())
    {
      int x, y, z;
      double v;
      in >> x >> y >> z >> v;
      f(x,y,z) = v;
    }

  arma::Cube<int> cc_f(N,N,N);
  cc_f.fill(0);
  std::map<int, int> w;
  mark_cc(f, cc_f, w);

  std::ofstream out(argv[3]);
  if (! out.is_open())
    throw std::runtime_error("Cannot open file");

  for (auto& c : w)
    {
      if (c.first != 0)
        out << c.first << " " << c.second << "\n";
    }

  std::ofstream out2(argv[4]);
  if (! out.is_open())
    throw std::runtime_error("Cannot open file");

  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      for (int k = 0; k < N; ++k)
        out2 << i << " " << j << " " << k << " " << cc_f(i,j,k) << "\n";
}
