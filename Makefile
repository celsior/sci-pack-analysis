.PHONY=all

all : main main_2d gr conn_comp sep_dist

main: main.cpp
	g++ -g --std=c++11 -o main -frounding-math -O4 main.cpp -lm -lCGAL -lCGAL_Core -lgmp -larmadillo

main_2d: main_2d.cpp
	g++ -g --std=c++11 -o main_2d -frounding-math -O4 main_2d.cpp -lm -lCGAL -lCGAL_Core -lgmp -larmadillo

gr: gr.cpp
	g++ -g --std=c++11 -o gr -O4 gr.cpp -lm

conn_comp: conn_comp.cpp
	g++ -g --std=c++11 -o conn_comp -O4 conn_comp.cpp -lm -larmadillo

sep_dist: sep_dist.cpp
	g++ -g --std=c++11 -o sep_dist -O4 sep_dist.cpp -lm
