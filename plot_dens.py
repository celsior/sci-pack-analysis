#!/usr/bin/python

import sys

import numpy as np

from mayavi import mlab

side = float(sys.argv[1])
N = int(sys.argv[2])
dx = side/N

#x,y,z = np.mgrid[0:x_steps, 0:y_steps, 0:z_steps]
#s = [[[0 for i in xrange(z_steps)] for j in xrange(y_steps)] for k in
#xrange(x_steps)]

mx = -1e15
mn = 1e15

s = np.empty( (N,N,N) )
for ln in open(sys.argv[3]):
    #i, j, k, v1, v2, v3 = ln.split()
    x, y, z, v1 = ln.split()
    try:
        v = float(v1)
        s[int( float(x)/dx ), int( float(y)/dx ), int( float(z)/dx )] = 1 if v == -1 else 0
        mx = max(v, mx)
        mn = min(v, mn)
    except:
        sys.stderr.write("%s %s %s %s" % (x,y,z,v1))
        exit(1)

print mx, mn

mlab.pipeline.volume(mlab.pipeline.scalar_field(s), vmax = 1)
mlab.pipeline.image_plane_widget(mlab.pipeline.scalar_field(s),
                            plane_orientation='x_axes',
                            slice_index=50,
                        )
mlab.pipeline.image_plane_widget(mlab.pipeline.scalar_field(s),
                            plane_orientation='z_axes',
                            slice_index=50,
                        )
mlab.outline()
mlab.show()
