#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
#include <stdexcept>


double sqr(double v)
{
  return v*v;
}

double cbk(double v)
{
  return v*v*v;
}

struct Point
{
  double x,y,z;
  Point(double x, double y, double z) : x(x), y(y), z(z) {};
};

typedef std::vector< Point > ParticlesVector;

void read_particles(const char* fname, double side, ParticlesVector& points)
{
  std::ifstream str(fname);
  if (! str.is_open())
    throw std::runtime_error("Cannot open file");

  char rectype = 0;
  str >> rectype;
  while (rectype != 'E')
    {
      if (rectype == 'E')
        break;
      else if (rectype != 'P')
        throw std::runtime_error("Malformed file");

      std::string tag, distr;
      double radius;
      bool fixed;
      double x, y, z;

      str >> tag >> distr >> x >> y >> z >> radius >> fixed;
      points.push_back(Point(x,y,z));

      if (str.eof())
        break;

      str >> rectype;
    }
}

int main(int argc, const char** argv)
{
  double L = atof(argv[1]);
  int bins = atoi(argv[2]);
  std::vector<double> g(bins, 0);
  std::vector<double> k(bins, 0);

  double dr = L/3/bins;
  int N = 0;

  int samples = (argc-3);
  int S = 0;
  for (int i = 0; i < samples; ++i)
    {
      ParticlesVector particles;
      read_particles(argv[i+3], L, particles);

      if (particles.size() == 0)
        throw std::runtime_error(std::string("Empty file ") + argv[i+3]);

      std::cerr << i+1 << " / " << samples << "  " << particles.size() << " " << argv[i+3] << "\n";

      for (int i = 0; i < particles.size(); ++i)
        {
          if (particles[i].x > 0 && particles[i].x < L &&
              particles[i].y > 0 && particles[i].y < L &&
              particles[i].z > 0 && particles[i].z < L)
            N += 1;

          if (particles[i].x < L/3 || particles[i].x > 2*L/3 ||
              particles[i].y < L/3 || particles[i].y > 2*L/3 ||
              particles[i].z < L/3 || particles[i].z > 2*L/3)
            continue;

          S += 1;
          for (int j = i; j < particles.size(); ++j)
            {
              double dx = particles[i].x - particles[j].x;
              double dy = particles[i].y - particles[j].y;
              double dz = particles[i].z - particles[j].z;
              double rij = sqrt(sqr(dx) + sqr(dy) + sqr(dz));

              if (rij < L/3)
                g[(int)(rij/dr)] += 2.0;
            }
        }
    }

  double rho = N / cbk(L);


  for (int i = 1; i < bins; ++i)
    k[i] = k[i-1] + g[i];

  for (int i = 1; i < bins; ++i)
    {
      g[i] /= 4.0 * M_PI/3.0 * (cbk(i+1)-cbk(i))*cbk(dr) *  (rho*S/samples);
      k[i] /= rho*S/samples;
      double l_i = cbrt(3*k[i] / 4.0 / M_PI) - (i+0.5)*dr;

      std::cout << (i+0.5)*dr << " " << g[i] << " " << k[i] << " " << l_i << "\n";
    }
}
