include "/home/celsior/study/styles/preamble.asy";

import markers;
import graph;
import palette;
import contour;

size(8cm,8cm);

void draw_file(string fname, pen p=currentpen)
{
  file f = input(fname);
  while (! eof(f))
    {
      string type = f.line().word();
      if (type == "p")
        {
          pair v = f;
          dot(v, p=p);
        }
      else if (type == "t")
        {
          pair v1 = f;
          pair v2 = f;
          pair v3 = f;
          draw(v1--v2--v3--cycle, p=p);
        }
      else if (type == "ft")
        {
          pair v1 = f;
          pair v2 = f;
          pair v3 = f;
          filldraw(v1--v2--v3--cycle, fillpen=p);
        }

      else if (type == "c")
        {
          pair c = f;
          real r = f;
          draw(Circle(c, r), p=p);
        }
      else if (type == "fc")
        {
          pair c = f;
          real r = f;
          filldraw(Circle(c, r), fillpen=p);
        }

    }
}

draw_file("tr");
// draw_file("coinc1", lightgray);
// defaultpen(0.1);
// draw_file("triang1");
// defaultpen(0.75);
// limits((0,0),(500,500),Crop);

// xaxis(BottomTop,LeftTicks(),above=true);
// label(shift(0mm,-6mm)*"$x$, мкм", point(SE));
// yaxis(LeftRight,RightTicks(),above=true);
// label(shift(0,3mm)*"$y$, мкм", point(NW));
