#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <tuple>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>

#include <CGAL/IO/Geomview_stream.h>
#include <CGAL/IO/Triangulation_geomview_ostream_2.h>

#include <CGAL/Point_2.h>

#include <armadillo>

// typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
// typedef CGAL::Point_2<K> Point;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<double, K> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb> Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds> Delaunay;
typedef Delaunay::Point Point;

typedef std::vector< std::pair<Point, double> > CirclesVector;

double sqr(double v)
{
  return v*v;
}

double sign(double v)
{
  if (v == 0)
    return 0;
  else if (v < 0)
    return -1;
  return 1;
}

void read_particles(const char* fname, CirclesVector& points)
{
  std::ifstream str(fname);
  if (! str.is_open())
    throw std::runtime_error("Cannot open file");

  std::string tmp;
  char rectype = 0;
  str >> rectype;
  while (!str.eof() && (rectype != 'E') )
    {
      if (rectype == 'E')
        break;
      else if (rectype != 'P')
        throw std::runtime_error("Malformed file");

      std::string tag, distr;
      double radius;
      bool fixed;
      double x, y, z;

      str >> tag >> distr >> x >> y >> z >> radius >> fixed;
      points.push_back(std::make_pair(Point(x,y), radius));

      // if (tag == "AP")
      //   points.push_back(Point(x,y,z));

      str >> rectype;
    }
}

double field_value(const CirclesVector& circles, const Point& p)
{
  double ret = 0;
  for (auto& c : circles)
    {
      double d = sqrt( sqr(c.first.x() - p.x()) +  sqr(c.first.y() - p.y()) ) - c.second;
      if (d < 0)
        return -1;
      ret += 1.0/d;
    }
  return ret;
}


bool in_triangle(const Point& p, const Point& v1, const Point& v2, const Point& v3)
{
  double alpha = ((v2.y() - v3.y())*(p.x() - v3.x()) + (v3.x() - v2.x())*(p.y() - v3.y())) /
    ((v2.y() - v3.y())*(v1.x() - v3.x()) + (v3.x() - v2.x())*(v1.y() - v3.y()));
  double beta = ((v3.y() - v1.y())*(p.x() - v3.x()) + (v1.x() - v3.x())*(p.y() - v3.y())) /
    ((v2.y() - v3.y())*(v1.x() - v3.x()) + (v3.x() - v2.x())*(v1.y() - v3.y()));
  double gamma = 1.0 - alpha - beta;
  return (alpha > 0 && beta > 0 && gamma > 0);
}

void write_circles(const char* fname, const CirclesVector& circles)
{
  std::ofstream out(fname);
  for (auto& c : circles)
    out << "c " << c.first.x() << "," << c.first.y() << " " << c.second << "\n";
}


int unify(int l1, int l2, std::vector<int>& eq, std::vector<bool>& single_eq)
{
  if (l1 != 0 && l2 != 0 && (eq[l1] != eq[l2]))
    {
      if (single_eq[eq[l1]])
        {
          eq[l1] = eq[l2];
          single_eq[eq[l2]] = false;
          return l2;
        }

      if (single_eq[eq[l2]])
        {
          eq[l2] = eq[l1];
          single_eq[eq[l1]] = false;
          return l1;
        }

      for (int n = 0; n <= eq.size()-1; ++n)
        if (eq[n] == eq[l1])
          eq[n] = eq[l2];
      return l2;
    }

  if (l1 != 0)
    return l1;
  if (l2 != 0)
    return l2;

  return 0;
}

void eq_unify(int e1, int e2, std::vector<int>& eq)
{
  if (e1 != 0 && e2 != 0 && (e1 != e2))
    for (int n = 0; n <= eq.size()-1; ++n)
      if (eq[n] == e1)
        eq[n] = e2;
}

void fill_map(const Delaunay& T, double side, arma::mat& f)
{
  int Nx = f.n_rows;
  int Ny = f.n_cols;
  double dx = side / Nx;
  double dy = side / Ny;

  for (auto fit = T.finite_faces_begin(); fit !=  T.finite_faces_end(); ++fit)
    {
      auto bb = T.triangle(fit).bbox();

      for (int i = floor(bb.xmin()/dx); i < ceil(bb.xmax()/dx);  ++i)
        for (int j = floor(bb.ymin()/dy); j < ceil(bb.ymax()/dy); ++j)
            {
              Point p(side/Nx*i, side/Ny*j);
              if (p.x() < 0 || p.y() < 0 || p.x() > side || p.y() > side || i >= Nx || j >= Ny)
                continue;

              auto triang = T.triangle(fit);
              if (! in_triangle(p,  triang[2],  triang[1],  triang[0]))
                continue;

              double ret = 0;
              for (int n = 0; n < 3; ++n)
                {
                  Point cr = fit->vertex(n)->point();
                  double r = fit->vertex(n)->info();

                  double d = sqrt( sqr(cr.x() - side/Nx*i) +  sqr(cr.y() - side/Ny*j) ) - r;
                  if (d < 0)
                    ret = -1e25;
                  else
                    ret += 1.0/d;
                  //ret = d < 0 || ret < 0 ? -1 : std::max(ret, pow(1.0/d, 0.5));
                }
              for (int n = 0; n < 3; ++n)
                {
                  Point cr = T.mirror_vertex(fit, n)->point();
                  double r = T.mirror_vertex(fit, n)->info();

                  double d = sqrt( sqr(cr.x() - side/Nx*i) +  sqr(cr.y() - side/Ny*j) ) - r;
                  if (d < 0)
                    ret = -1e25;
                  else
                    ret += 1.0/d;
                  //ret = d < 0 || ret < 0 ? -1 : std::max(ret, pow(1.0/d, 0.5));
                }
              // if (f(i,j) != 0)
              //   std::cerr << "overwrite " << i << " " << j << " " << f(i,j) << " " << ret << "\n";
              f(i,j) = (ret > 0 ? ret : -1);
            }
    }
}

void mark_clusters(arma::mat& f, arma::Mat<int>& cluster_f, std::map<int, int>& w)
{
  int Nx = f.n_rows;
  int Ny = f.n_cols;

  std::vector<int> eq = {0};
  std::vector<bool> single_eq = {false};
  int max_label = 0;


  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Ny; ++j)
        {
          if (f(i,j) == -1 || f(i,j) > 0.12)
            continue;

          int li = i == 0 ? 0 : cluster_f(i-1, j);
          int lj = j == 0 ? 0 : cluster_f(i, j-1);
          cluster_f(i,j) = unify(li, lj, eq, single_eq);
        }

  for (int i = 0; i < Nx; ++i)
      {
        int li = cluster_f(0,i);
        int lj = cluster_f(Nx-1,i);
        unify(li, lj, eq, single_eq);
      }

  for (int i = 0; i < Ny; ++i)
      {
        int li = cluster_f(i,0);
        int lj = cluster_f(i,Ny-1);
        unify(li, lj, eq, single_eq);
      }

  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Ny; ++j)
      {
        cluster_f(i,j) = eq[cluster_f(i,j)];
        w[cluster_f(i,j)] += 1;
      }
}

void find_min(arma::mat& f, std::vector< std::tuple<int,int> >& ret)
{
  int Nx = f.n_rows;
  int Ny = f.n_cols;

  for (int i = 0; i < Nx; ++i)
    for (int j = 0; j < Ny; ++j)
        {
          double fm = f(i,j);
          if (fm > 0.12)
            continue;

          ret.push_back(std::make_tuple(i,j));
        }
}

int main(int argc, const char** argv)
{
  std::cerr << "reading\n";
  CirclesVector circles;
  read_particles(argv[1], circles);

  //write_circles("circ1", circles);

  const int N = 200;
  const double side = 500;
  const double dx = side/N;

  Delaunay T;
  T.insert( circles.begin(), circles.end() );

  //write_triangulation("triang1", T);
  //write_coincident_circles("coinc1", T);

  std::cerr << "building\n";
  arma::mat f(N, N);
  f.fill(0);
  fill_map(T, side, f);

  std::cerr << "cluster\n";
  arma::Mat<int> cluster_f(N,N);
  cluster_f.fill(0);
  std::map<int, int> w;
  mark_clusters(f, cluster_f, w);

  // std::vector< std::tuple<int,int> > mn;
  // find_min(f, mn);
  // for (auto& p : mn)
  //   std::cerr << "p " << std::get<0>(p) << " " <<  std::get<1>(p) << "\n";

  for (auto& c : w)
    {
      if (c.first != 0)
        std::cerr << c.first << " " << c.second << "\n";
    }

  for (int i = 0; i < f.n_cols; ++i)
    for (int j = 0; j < f.n_rows; ++j)
      {
        std::cout << side/N*i << " " << side/N*j << " " << f(i,j) << "\n";
        //std::cout << side/N*i << " " << side/N*j << " " << side/N*k << " " << (cluster_f(i,j,k) ? cluster_f(i,j,k)*5+100 : 0)  << "\n";
      }


  return 0;
}
