include "/home/celsior/study/styles/preamble.asy";

size(8.5cm,5cm,IgnoreAspect);

real[][] d1 = read_data(fname);

pair[] data = new pair[d1[0].length];
for (int i = 0; i < d1[0].length; ++i)
  data[i] = ((d1[0][i]+d1[1][i])/2, d1[2][i]);

draw(graph(data));

xlimits(lx.x, lx.y, Crop);
ylimits(ly.x, ly.y, Crop);

xaxis(BottomTop,LeftTicks());
label(shift(0mm,-6mm)*"$r$, мкм", point(SE));
yaxis(LeftRight,RightTicks());
label(shift(0,3mm)*"$p$", point(NW));
