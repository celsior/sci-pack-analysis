include "/home/celsior/study/styles/preamble.asy";

import markers;
import graph;
import palette;
import contour;

size(8cm,8cm,IgnoreAspect);

file in=input("dens_test_0").line();
real[][] a=in.dimension(0,0);
real side = 100;
int N = 50;

real[][] v=new real[N][N];

real maxv = 0;
real minv = 1e25;

for (int i = 0; i < a.length; ++i)
  {
    real val = a[i][2] == -1 ? -1 : sqrt(a[i][2]);
    v[a[i][0]][a[i][1]] = val;

    if (val != -1)
      {
        minv = min(minv, val);
        maxv = max(maxv, val);
      }
  }

write(maxv, minv);

pen[] palette = new pen[32000];
real step = 1.0/32000;
for (int i = 0; i < 32000; ++i)
  {
    real v = i*step;
    palette[i] = rgb(abs(2*v - 0.5), sin(pi*v), cos(pi/2*v));
    //palette[i] = gray(1 - sqrt(v));
  }


bounds range=image(v, (0,0),(side,side), palette, range=Range(minv, maxv));

//bounds range=image(v, (0,0),(side,side), palette);

draw(contour(v, (0,0), (side,side), new real[]{3}));
//draw_struct("out_idx");

limits( (0,0), (side,side), Crop );

xaxis(BottomTop,LeftTicks(),above=true);
label(shift(0mm,-6mm)*"$x$, мкм", point(SE));
yaxis(LeftRight,RightTicks(),above=true);
label(shift(0,3mm)*"$y$, мкм", point(NW));


// picture bar;
// palette(bar,"",range,(0,0),(0.5cm, 9cm),Right, palette, PaletteTicks(defaultformat, Step=10));
// attach(bar.fit(),point(E),10E);
// label(shift(6mm,-0mm)*"$T$, K", point(NE));
