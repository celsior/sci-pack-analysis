#!/bin/bash

side=$1
bname=${2%%.dat}
sep_dist=1
min_area=900
min_diameter=15
grid_cells=300
bins=300

cell_vol=$(python -c "print (($1+0.0)/$grid_cells)**3")

mkdir $bname

awk '{ if ($2 == "Al") print $0 }' "$bname".dat > "$bname"/Al.dat
./gr $side $bins "$bname".dat > "$bname"/gr.dat
./gr $side $bins "$bname"/Al.dat > "$bname"/Al_gr.dat

./sep_dist $side $sep_dist "$bname".dat "$bname"/nn.dat /dev/null
./sep_dist $side $sep_dist "$bname"/Al.dat "$bname"/Al_nn.dat "$bname"/cls.dat

./hist.py "$bname"/nn.dat --bins $bins --col 3 --normed > "$bname"/pp_dist.dat
./hist.py "$bname"/nn.dat --bins $bins --col 4 --normed > "$bname"/cc_dist.dat
./hist.py "$bname"/Al_nn.dat --bins $bins --col 3 --normed > "$bname"/Al_pp_dist.dat
./hist.py "$bname"/Al_nn.dat --bins $bins --col 4 --normed > "$bname"/Al_cc_dist.dat
./hist.py "$bname"/cls.dat --bins $bins --col 1 --normed --type vol --cluster --mass > "$bname"/cls_dist.dat

awk '{ if ($2 != "Al") print $0 }' "$bname".dat > "$bname"/AP.dat
./main $side $grid_cells $min_area $min_diameter "$bname"/AP.dat "$bname"/dens.dat
./conn_comp $grid_cells "$bname"/dens.dat "$bname"/pockets.dat "$bname"/pockets_map.dat

./hist.py "$bname"/pockets.dat --cellvolume $cell_vol --bins $bins --col 1 --normed --type vol --mass > "$bname"/pockets_dist.dat
